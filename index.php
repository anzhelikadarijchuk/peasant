<!DOCTYPE html>

<html>

<head>
    <title>ewwww definition</title>    
    <link rel="stylesheet" type="text/css" href="<?php echo(get_stylesheet_uri()); ?>">
    <link rel="icon" href="/capstone/favicon (1).ico" type="image/x-icon">
    <meta id="viewport" name="viewport" content ="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>

<body>

    <!-- Header -->

    <header>
        <div class="logo"><img src="<?php the_template_directory_uri(); ?>/capstone/ic-logo.svg" alt="best school" /> 
        <h2>e<span>wwww</span></h2>
    </div>
        
        <nav>
           <ul role="presentation">
               <li class="blue"><a href="#">ew online</a></li>
               <li class="blue"><a href="#">eww courses</a></li>
               <li class="blue"><a href="#">ewww</a></li>
               <li><a href="#">Sign in</a></li>
           </ul>
       </nav>

       <nav class="alt-nav" role="presentation">
           <ul>
               <li><a href="#"><img src="<?php the_template_directory_uri(); ?>/capstone/ic-logo.svg" /></a></li>
               <li><a href="#"><img src="<?php the_template_directory_uri(); ?>/capstone/ic-on-campus.svg" /></a></li>
               <li><a href="#"><img src="<?php the_template_directory_uri(); ?>/capstone/ic-online.svg" /></a></li>
               <li><a href="#"><img src="<?php the_template_directory_uri(); ?>/capstone/ic-login.svg" /></a></li>
               
           </ul>
        </nav>

    </header>

    <!-- Banner Section -->

    <div class="banner">
        <div class="learn">
            <img src="<?php the_template_directory_uri(); ?>/capstone/banner.jpg" alt="eww word pronunciation"/>
            <div class="banner-text" role="presentation">
                <h1>Learn something new everyday</h1>
                <p><strong>Ew</strong> is a favorite authors word. So, today we proudly introduce you the ewwww word! That's right, with 4 "W"s</p>
                <button>Start here</button>
            </div>
        </div>
    </div>

    <!-- Main -->

    <div class="main">
        <div class="sub-main">
        <div class="main-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/information-main.jpg" alt="best college" />
            <div class="main-container-text" role="presentation">
                <h3>It doesn't hurt to keep practicing ewwww pronunciation</h3>
                <p>"Ewww pronunciation is different from ew pronunciation. But do not worry! 
                    It is easier than it seems. Just make your u longer: uuuuuuuuu — like this!"</p>
                <p id="author">Emanuel, ewww word Enthusiast, ewww Pronunciation Evangelist</p>  
            </div>
        </div>
    

        <!-- <div class="border"> -->
        <div class="main-container-right">
          <div class="main-sub-container">
              <img class="img-desktop" src="<?php the_template_directory_uri(); ?>/capstone/information-orientation.jpg" alt="best-school" />
              <img class="img-mobile" src="<?php the_template_directory_uri(); ?>/capstone/information-orientation-mobile.jpg" alt="Orientation-date" />
              <div class="main-text">
                  <h3>Orientation date</h3>
                  <p>Tue 10/11 & Wed 10/12: 8am-3pm</p>
                  <a href="#">Read more</a>
              </div>
          </div> 

            <div class="main-sub-container">
                <img class="img-desktop" src="<?php the_template_directory_uri(); ?>/capstone/information-campus.jpg" alt="best-school" />
                <img class="img-mobile" src="<?php the_template_directory_uri(); ?>/capstone/information-campus-mobile.jpg" alt="Orientation-date" />
                <div class="main-text">
                    <h3>Our campus</h3>
                    <p>Find which ewww word is closer to you</p>
                    <a href="#">Read more</a>
                </div>
            </div>
                        
            <div class="main-sub-container" id="guest">
                    <img class="img-desktop" src="<?php the_template_directory_uri(); ?>/capstone/information-guest-lecture.jpg" alt="guest-lecture" />
                    <img class="img-mobile" src="<?php the_template_directory_uri(); ?>/capstone/information-guest-lecture-mobile.jpg" alt="Orientation-date" />
                    <div class="main-text">
                        <h3>Our guest lecture</h3>
                        <p>Join a keynote with Oliver Ew about ewwww music in medical treatment</p>
                        <a href="#">Read more</a>
                    </div>
            </div> 
        </div>
        <!-- </div> -->
    </div>
    </div>

<!-- Courses -->

<div class="courses-container">
    <h2>Start learning</h2>
    <div class="courses">
        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-software.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ew Serial Entrepreneur</h3>
            <p class="courses-p">COURSES</p>
            <P>Here you will learn how to be a serial ew word entrepreneur. That's every business you will start might fail.</P></div>
        </div>

        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-computer-art.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ew Founder</h3>
            <p class="courses-p">COURSES</p>
            <P>How to become a new ew word founder</P></div>
        </div>

        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-design.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ew Philantropy</h3>
            <p class="courses-p">COURSES</p>
            <P>You will learn hor to be an authentic ewww word philantropist</P></div>
        </div>

        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-data.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ew Data</h3>
            <p class="courses-p">COURSES</p>
            <P>Ew Data Science, Ewww Big Data, Ewww Visualization</P></div>
        </div>

        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-business.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ewwww Business</h3>
            <p class="courses-p">COURSES</p>
            <P>Ew Product Development, Ewww Business Development, Ewww Startup</P></div>
        </div>

        <div class="course-container">
            <img src="<?php the_template_directory_uri(); ?>/capstone/course-marketing.jpg" alt="best software engineering university" />
            <div class="course-container-text"><h3>Ewww Marketing</h3>
            <p class="courses-p">COURSES</p>
            <P>Eww Analytics, Eww Content Marketing</P></div>
        </div>
    </div>
</div>

<div class="courses-mobile">
    <a href="#"><h3>Ew Serial Entrepreneur</h3></a>
    <a href="#"><h3>Ew Founder</h3></a>
    <a href="#"><h3>Ew Philantropy</h3></a>
    <a href="#"><h3>Ew Data</h3></a>
    <a href="#"><h3>Ewwww Business</h3></a>
    <a href="#"><h3>Ewww Marketing</h3></a>
</div>

<!-- Thesis part -->

<div class="thesis-container">
    <h2 id="title">Thesis exhibit</h2>
    <div class="thesis-elements">
        <video src="<?php the_template_directory_uri(); ?>/capstone/thesis.mp4" controls>Video not supported</video>
        <div class="thesis-right-containers">
            <div class="thesis-right-container" id="first">
                <img src="<?php the_template_directory_uri(); ?>/capstone/thesis-fisma.jpg" alt="best-university">
                <div class="text-thesis"><h3>Fisma: Design and Prototype</h3>
                     <p>Designer showcase of new prototype product</p>
                    </div>
            </div>
            <div class="thesis-right-container">
                <img src="<?php the_template_directory_uri(); ?>/capstone/thesis-now-and-then.jpg" alt="best-university">
                <div class="text-thesis"><h3>Now and then</h3>
                <p>Research study about New York</p>
                </div>
            </div>
        </div>
    </div>
    <div class="reimagine">
        <h3>Reimagine urban</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a 
            galley of type and scrambled it to make a type specimen book. It is a long established fact that a reader will
             be distracted by the readable content of a page when looking at its layout.</p>
    </div>
</div>

<footer>
    <div class="container-footer">
        <p>&#169; 2016 Colmar Academy. All rights reserved</p>
        <p class="footer-p"><a href="#">Terms</a> <a href="#">Privacy</a></p>

    </div>
</footer>
</body>

</html>